'use strict';
const { Op, sequelize } = require("sequelize");
const { validationResult } = require('express-validator');
var models = require('../models/');
var marca = models.marca;
var auto = models.auto;
var rol = models.rol;

class AutoController {
    async listar(req, res) {
        var lista = await auto.findAll({
            where: { duenio: 'Sin_Duenio' },
            include: { model: models.marca, foreignKey: 'id_marca', attributes: ['nombre', 'pais_origen'] },
            attributes: ['modelo', 'anio', 'cilindraje', 'color', 'precio', 'external_id']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    /*async vendidos(req, res) {
        var lista = await auto.findAll({
            where: { duenio: {[Op.ne]:'Sin_Duenio' }},
            include: { model: models.marca, foreignKey: 'id_marca', attributes: ['nombre', 'pais_origen'] },
            attributes: ['modelo', 'anio', 'cilindraje', 'color', 'precio', 'external_id']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }*/

    async vendidos(req, res) {
        var lista = await models.sequelize.query(
            //'SELECT * FROM auto INNER JOIN persona ON auto.duenio = persona.identificacion',
            "SELECT auto.modelo, auto.anio, auto.cilindraje, auto.color, persona.nombres, persona.apellidos FROM auto INNER JOIN persona ON auto.duenio = persona.identificacion",
            { type: models.Sequelize.QueryTypes.SELECT }
        );
        if (lista === null) {
            res.status(200);
            res.json({ msg: "No existen datos registrados", code: 200, info: lista });
        } else {
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        }

    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await auto.findOne({
            where: { external_id: external },
            include: { model: models.marca, foreignKey: 'id_marca', attributes: ['nombre', 'pais_origen'] },
            //include: { model: models.marca, attributes: ['nombre', 'pais_origen'] },
            attributes: ['modelo', 'anio', 'cilindraje', 'color', 'precio']
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {
        console.log(req.body);
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                console.log(marcaAux);
                if (marcaAux) {
                    var data = {
                        modelo: req.body.modelo,
                        anio: req.body.anio,
                        cilindraje: req.body.cilindraje,
                        color: req.body.color,
                        precio: req.body.precio,
                        id_marca: marcaAux.id,
                    };
                    res.status(200);
                    //let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data);
                        //await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    async cantAutos(req, res) {
        var cant = await auto.count();
        res.json({msg: "OK!", code: 200, info: cant});
    }

    async colores(req, res) {
        var lista = await auto.rawAttributes.color.values;
        console.log(lista);
        res.json({msg: "OK!", code: 200, info: lista});
    }

    async modificar(req, res) {
        var carro = await auto.findOne({ where: { external_id: req.body.external, Duenio: "Sin_Duenio" } });
        if (carro === null) {
            res.status(400);
            res.json({ msg: "No existe el registro o el auto ya fue vendido", code: 400 });
        } else {
            var uuid = require('uuid');
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                if (marcaAux) { 
                    carro.modelo = req.body.modelo;
                    carro.anio = req.body.anio;
                    carro.cilindraje = req.body.cilindraje;
                    carro.color = req.body.color;
                    carro.precio = req.body.precio;
                    carro.id_marca = marcaAux.id;
                    carro.external_id = uuid.v4();
                    var result = await carro.save();
                    if (result === null) {
                        res.status(400);
                        res.json({ msg: "No se ha modificado sus datos", code: 400 });
                    } else {
                        res.status(200);
                        res.json({ msg: "Se ha modificado sus datos", code: 200 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        }
    }
}

module.exports = AutoController;