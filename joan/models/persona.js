'use strict';
module.exports = (sequalize, DataTypes) => {
    const persona = sequalize.define('persona', {
        nombres: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        apellidos: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        identificacion: {
            type: DataTypes.STRING(20),
            unique: true,
            allowNull: false,
            defaultValue: "Sin_Datos"
        },
        tipo_identificacion: {
            type: DataTypes.ENUM("CEDULA","PASAPORTE","RUC"),
            allowNull: false,
            defaultValue: "CEDULA"
        }
        ,edad: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        telefono: {
            type: DataTypes.STRING(12),
            unique: true,
            defaultValue: "Sin_Datos"
        },
        direccion: {
            type: DataTypes.STRING,
            allowNull: true
        },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}
    }, {freezeTableName: true});
    persona.associate = function(models) {
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
        persona.hasMany(models.factura, {foreignKey: 'id_persona', as: 'factura'});
    }
    return persona;
};