'use strict';
module.exports = (sequalize, DataTypes) => {
    const detalle = sequalize.define('detalle', {
        subtotal: { type: DataTypes.DECIMAL(10, 2), defaultValue: 0.0 },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, { freezeTableName: true });
    detalle.associate = function (models) {
        detalle.belongsTo(models.factura, {foreignKey: 'id_factura'});
        //detalle.hasOne(models.auto, {foreignKey: 'id_detalle', as: 'auto'});
        detalle.belongsTo(models.auto, {foreignKey: 'id_auto'});
    }
    return detalle;
};