'use strict';
module.exports = (sequalize, DataTypes) => {
    const factura = sequalize.define('factura', {
        IVA: { type: DataTypes.DECIMAL(10, 2), defaultValue: 12.0 },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, { freezeTableName: true });
    factura.associate = function (models) {
        factura.belongsTo(models.persona, {foreignKey: 'id_persona'});
        factura.hasMany(models.detalle, {foreignKey: 'id_factura', as: 'detalle'});
    }
    return factura;
};