'use strict';
module.exports = (sequalize, DataTypes) => {
    const marca = sequalize.define('marca', {
        nombre: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        pais_origen : {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        external_id : {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, { freezeTableName: true });
    marca.associate = function (models) {
        marca.hasMany(models.auto, { foreignKey: 'id_marca', as: 'auto'});
    }
    return marca;
};