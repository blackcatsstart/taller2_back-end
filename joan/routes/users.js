var express = require('express');
var router = express.Router();
const { body } = require('express-validator');
const RolController = require('../controls/RolController');
var rolController = new RolController();
const PersonaController = require('../controls/PersonaController');
var personaController = new PersonaController();
const MarcaController = require('../controls/MarcaController');
var marcaController = new MarcaController();
const AutoController = require('../controls/AutoController');
var autoController = new AutoController();
const FacturaController = require('../controls/FacturaController');
var facturaController = new FacturaController();
const CuentaController = require('../controls/CuentaContoller');
var cuentaController = new CuentaController();
let jwt = require('jsonwebtoken');
//middleware async
var auth = function middleware(req, res, next) {
  const token = req.headers['x-api-token'];
  //console.log(token);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log(err);
        res.status(401);
        res.json({ msg: "Token no valido o expirado!", code: 401});
      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido!", code: 401});
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe Token!", code: 401});
  }
}
//cuantos tipos de autentificacion hay en el restAPI
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "verssion": "1.0", "name": "auto" });
});
//roles
router.get('/roles', auth, rolController.listar);
//persona
router.post('/personas/guardar', auth, [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 50 }).withMessage("Ingrese un dato mayorigual a 3 y menor a 50"),
  body('nombres', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 50 }).withMessage("Ingrese un dato mayorigual a 3 y menor a 50")
], personaController.guardar);
router.post('/personas/modificar', auth, personaController.modificar);
router.get('/personas', auth, personaController.listar);
router.get('/personas/obtener/:external', personaController.obtener);

//Marca
router.get('/marcas', auth,marcaController.listar);
router.get('/marcas/cantidad', auth, marcaController.cantMarcas);
router.post('/marcas/guardar', auth, marcaController.guardar);

//Auto
router.get('/autos', autoController.listar);
router.get('/autos/colores', auth, autoController.colores);
router.get('/autos/cantidad', auth, autoController.cantAutos);
router.post('/autos/guardar', auth, autoController.guardar);
router.get('/autos/obtener/:external', autoController.obtener);
router.get('/autos/vendidos', auth, autoController.vendidos);
router.post('/autos/modificar', auth, autoController.modificar);
  
//Factura
router.post('/facturas/guardar', facturaController.guardar);
router.get('/facturas', facturaController.listar);
router.get('/facturas/obtener/:external', facturaController.obtener);

//Cuenta
router.post('/sesion', [
  body('email', 'Ingrese Correo valido').trim().exists().not().isEmpty().isEmail(),
  body('clave', 'Ingrese Clave valida').trim().exists().not().isEmpty()
], cuentaController.sesion);

/**
router.get('/sumar/:a/:b', function(req, res, next) {
  var a = req.params.a * 1;
  var b = Number(req.params.b);
  var c = a +b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});

router.post('/sumar', function(req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if(isNaN(a)||isNaN(b)){
    res.status(400);
    res.json({"msg":"Faltan Datos"});
  }
  console.log(b);
  var c = a +b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});*/

module.exports = router;
